#include <iostream>
#include <math.h>
#include <cstring>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <jsoncpp/json/reader.h>
#include <sstream>

constexpr char usage[] = "baresip_client [address port] command\n";

int ns_encode(const char *src, char **dst);
int ns_decode(const char *src, char **dst);

int main(int argc, char *argv[])
{
    const char *ip_addr = "127.0.0.1";
    int port = 4444;

    char *cmd = nullptr;
    char *netstring = nullptr;

    struct sockaddr_in server_addr = { 0 };
    int conn_fd;
    char input_buffer[64] = {0};
    int recvd_num = 0;
    std::stringstream answer;

    struct timeval tv{};
    tv.tv_sec = 0;
    tv.tv_usec = 900;

    if (argc < 2) {
        std::cerr << usage << std::endl;
        return 1;
    }
    if (argc < 4) {
        cmd = argv[1];
    } else {
        ip_addr = argv[1];
        port = strtol(argv[2], nullptr, 10);
        cmd = argv[3];
    }
    ns_encode(cmd, &netstring);

    server_addr.sin_port = htons(port);
    server_addr.sin_family = AF_INET;

    int ret = inet_pton(AF_INET, ip_addr, &server_addr.sin_addr);

    if (ret != 1) {
        std::cerr << "failed to convert address " << ip_addr << " to binary net address\n" << std::endl;
        return -1;
    }

    conn_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (conn_fd == -1) {
        std::cerr << "Failed to create socket" << std::endl;
        return -1;
    }
    setsockopt(conn_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    ret = connect(conn_fd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if (ret == -1) {
        std::cerr << "Failed to connect connect" << std::endl;
        return -1;
    }

    if (send(conn_fd, netstring, strlen(netstring), 0) == -1) {
        std::cerr << "Send error" << std::endl;
        close(conn_fd);
        return -1;
    }

    while (true) {
        if ((recvd_num=recv(conn_fd, input_buffer, sizeof(input_buffer) - 1, 0)) == -1) {
            break;
        }
        input_buffer[recvd_num] = '\0';
        answer << input_buffer;
        if (recvd_num == 0) {
            break;
        }
    }
    delete[](netstring);
    netstring = nullptr;
    ns_decode(answer.str().c_str(), &netstring);
    std::cout << netstring << std::endl << std::endl;

    close(conn_fd);
    return 0;
}
/**
 * Encode command to netstring for baresip
 * @param src source string
 * @param dst pointer to pointer to memory where encoded string will be.
 * @return 0 if OK
 */
int ns_encode(const char *src, char **dst) {
    if (!src) {
        return -1;
    }
    char *netstring = nullptr;
    size_t netstring_len = 0;
    size_t src_len = strlen(src);

    netstring_len = src_len + lround(log10(src_len)) + 1 + 3;
    netstring = new char[netstring_len];
    sprintf(netstring, "%lu:%s,", src_len, src);
    *dst = netstring;
    return 0;
}

/**
 * Simple netstring decoder, it's not optimal
 * @param src source netstring
 * @param dst pointer to pointer to memery where'll be clean string stored
 * @return 0 if OK
 */
int ns_decode(const char *src, char **dst) {
    if (!src) {
        return -1;
    }
    const char *str_beg = src;
    *dst = nullptr;
    char *cur_pos = nullptr;
    char *tmp;
    char *dst_ptr;
    size_t src_len = strlen(src);
    size_t str_len = 0;
    while(cur_pos - src != src_len) {
        str_len = strtoul(str_beg, &cur_pos, 10);
        if (str_len <- 0) {
            break;
        }
        if (cur_pos && *cur_pos) {
            cur_pos++;
        }
        if (!*dst) {
            *dst = new char[str_len + 1];
            dst_ptr = *dst;
        } else {
            tmp = *dst;
            *dst = new char[str_len + 1 + strlen(tmp)];
            strcpy(*dst, tmp);
            dst_ptr = *dst + strlen(*dst);
            delete[](tmp);
        }
        strncpy(dst_ptr, cur_pos, str_len);
        cur_pos += str_len;
        str_beg = cur_pos;

    }
    return 0;
}